﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardInfo : MonoBehaviour {

    public Text myTitle;
    public Text myEffect;
    public Text txtChild;
    public Text txtSpouse;
    public Text txtStaff;
    public Text txtHouse;
    public Text txtWits;

    Image myBackground;
    public Card myCard;

    public void UpdateCard(Card newCard)
    {
        myCard = newCard;
        myTitle.text = myCard.Title;
        myEffect.text = myCard.Effect;
        myBackground = this.GetComponent<Image>();
        if (myBackground != null)
        {
            myBackground.color = myCard.myBackgroundColor;
        }
        
        txtChild.text = myCard.Child.ToString("+###;-###;0");
        txtSpouse.text = myCard.Spouse.ToString("+###;-###;0");
        txtStaff.text = myCard.Staff.ToString("+###;-###;0");
        txtHouse.text = myCard.House.ToString("+###;-###;0");
        txtWits.text = myCard.Wits.ToString("+###;-###;0");     
    }

    public void UpdateCardForDementia(int Wits)
    {
        myTitle.text = UpdateStringForDementia(myTitle.text, Wits);
        myEffect.text = UpdateStringForDementia(myEffect.text, Wits);
        txtChild.text = UpdateStringForDementia(txtChild.text, Wits);
        txtSpouse.text = UpdateStringForDementia(txtSpouse.text, Wits);
        txtStaff.text = UpdateStringForDementia(txtStaff.text, Wits);
        txtHouse.text = UpdateStringForDementia(txtHouse.text, Wits);
        txtWits.text = UpdateStringForDementia(txtWits.text, Wits);
    }

    public string UpdateStringForDementia(string myString, int Wits)
    {
        string newString = "";
        foreach(char c in myString)
        {
            char newLetter = c;
            if (Random.Range(0.0f,1.0f)<=((10-Wits)*.03f))
            {
                int num = Random.Range(0, 27);
                if (num == 27)
                {
                    newLetter = ' ';
                }
                else {
                    newLetter = (char)('a' + num);
                }
                if (Random.Range(0.0f, 1.0f) <= .07f)
                {
                    newLetter = char.ToUpper(newLetter);
                }
            }

            newString = newString + newLetter;
        }
        Debug.LogWarning(myString + "=>" + newString);
        return newString;
    }
}

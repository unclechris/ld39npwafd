﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Playfield : MonoBehaviour {

    public Text TotalScore;
    public int currentTotalScore;
    public Text ChildScore;
    public int currentChildScore;
    public Text SpouseScore;
    public int currentSpouseScore;
    public Text StaffScore;
    public int currentStaffScore;
    public Text HouseScore;
    public int currentHouseScore;
    public Wits myWits;
    public int myWitsAdd;

    public YearlyEvent currentEvent;
    public EventCardInfo myEventCardInfo;
    public UpdateYearlyEvent myYearlyEvent;
    public GameManger myGameManager;
    public Transform myHand;
    public bool endOfTurn;
    public int TurnCount;

    public void UpdateScore(GameObject myCardObject)
    {
        CardInfo myCardInfo = myCardObject.GetComponent<CardInfo>();
        if (myCardInfo.myCard != null)
        {
            Card myNewCard = myCardInfo.myCard;
            myWitsAdd = myNewCard.Wits;
            myWitsAdd += UpdateChildScore(myNewCard.Child);
            myWitsAdd += UpdateSpouseScore(myNewCard.Spouse);
            myWitsAdd += UpdateStaffScore(myNewCard.Staff);
            myWitsAdd += UpdateHouseScore(myNewCard.House);
            myWitsAdd = Mathf.Clamp(myWitsAdd, -5, +5);
            UpdateTotalScore();
            CheckEvent();
            UpdateWits(myWitsAdd);
            if (myWits.CurrentWits<5)
            {
                ScrambleCards(transform);
                ScrambleCards(myHand);
                myEventCardInfo.UpdateEventForDementia(myWits.CurrentWits);
                myCardInfo.UpdateCardForDementia(myWits.CurrentWits);
            }
        }
    }
    
    private void ScrambleCards(Transform myTransform)
    {
        foreach (Transform child in myTransform)
        {
            CardInfo myCardInfo = child.gameObject.GetComponent<CardInfo>();
            if (myCardInfo != null)
            {
                myCardInfo.UpdateCardForDementia(myWits.CurrentWits);
            }
        }
        
    }

    public void EndTurn()
    {
        TurnCount += 1;
        //Remove All Cards on Field
        RemoveAllCurrentCards();
        //Display Score Screen
        myGameManager.SetStateEndOfTurn();
        //Get New Event
        Destroy(myEventCardInfo.transform.gameObject);
        currentEvent = null;
        myEventCardInfo = null;
        myYearlyEvent.HasEvent = false;
        myYearlyEvent.UpdateYearlyEventCard();
        endOfTurn = false;
    }
    private void RemoveAllCurrentCards()
    {
        foreach (Transform child in transform)
        {
            DestroyObject(child.gameObject);
        }
        DropZone myDropInfo = transform.GetComponent<DropZone>();
        if (myDropInfo != null)
        {
            myDropInfo.NumOfItems = 0;
        }
    }

    private void CheckEvent()
    {
        int scoreToCompareToGoal = 0;
        if (currentEvent.Complete == true)
            return;
        switch (currentEvent.Goal)
        {
            case YearlyEvent.TypeOfInfluence.CHILD:
                scoreToCompareToGoal = currentChildScore;
                break;
            case YearlyEvent.TypeOfInfluence.HOUSE:
                scoreToCompareToGoal = currentHouseScore;
                break;
            case YearlyEvent.TypeOfInfluence.SPOUSE:
                scoreToCompareToGoal = currentSpouseScore;
                break;
            case YearlyEvent.TypeOfInfluence.STAFF:
                scoreToCompareToGoal = currentStaffScore;
                break;
            case YearlyEvent.TypeOfInfluence.WITS:
                scoreToCompareToGoal = myWits.CurrentWits;
                break;
            default:
                break;
        }
        if (scoreToCompareToGoal >= currentEvent.GoalScore)
        {
            currentEvent.Complete = true;
            switch (currentEvent.Success)
            {
                case YearlyEvent.TypeOfInfluence.CHILD:
                    UpdateChildScore(currentEvent.SuccessBonus);
                    break;
                case YearlyEvent.TypeOfInfluence.HOUSE:
                    UpdateHouseScore(currentEvent.SuccessBonus);
                    break;
                case YearlyEvent.TypeOfInfluence.SPOUSE:
                    UpdateSpouseScore(currentEvent.SuccessBonus);
                    break;
                case YearlyEvent.TypeOfInfluence.STAFF:
                    UpdateStaffScore(currentEvent.SuccessBonus);
                    break;
                case YearlyEvent.TypeOfInfluence.WITS:
                    myWitsAdd += currentEvent.SuccessBonus;
                    break;
                default:
                    break;
            }           
        }
        if (endOfTurn == true && scoreToCompareToGoal < currentEvent.GoalScore)
        {
            switch (currentEvent.Failure)
            {
                case YearlyEvent.TypeOfInfluence.CHILD:
                    UpdateChildScore(currentEvent.FailureReward);
                    break;
                case YearlyEvent.TypeOfInfluence.HOUSE:
                    UpdateHouseScore(currentEvent.FailureReward);
                    break;
                case YearlyEvent.TypeOfInfluence.SPOUSE:
                    UpdateSpouseScore(currentEvent.FailureReward);
                    break;
                case YearlyEvent.TypeOfInfluence.STAFF:
                    UpdateStaffScore(currentEvent.FailureReward);
                    break;
                case YearlyEvent.TypeOfInfluence.WITS:
                    myWitsAdd += currentEvent.FailureReward;
                    break;
                default:
                    break;
            }
        }

        if (myEventCardInfo != null)
        {
            myEventCardInfo.UpdateEventCardInfo();
        }

    }

    private void UpdateWits(int myWitsAdd)
    {
        Debug.Log("UpdateWits"+myWitsAdd.ToString("+###;-###;0"));
        myWits.UpdateWits(myWitsAdd);
    }

    private void UpdateTotalScore()
    {
        currentTotalScore = currentChildScore + currentHouseScore + currentSpouseScore + currentStaffScore;
        TotalScore.text = currentTotalScore.ToString("+###;-###;0");        
    }

    private int UpdateChildScore(int child)
    {
        int myWitsAdd = 0;
        currentChildScore += child;
        if (currentChildScore < -10)
        {
            myWitsAdd -= Mathf.Max(5, -(currentChildScore + 10));
            currentChildScore = -10;
        }
        if (currentChildScore > 10)
        {
            myWitsAdd += Mathf.Max(5, currentChildScore - 10);
            currentChildScore = 10;
        }
        ChildScore.text = currentChildScore.ToString("+##;-##;0");
        myWitsAdd = Mathf.Clamp(myWitsAdd, -5, +5);
        return myWitsAdd;
    }

    private int UpdateSpouseScore(int Spouse)
    {
        int myWitsAdd = 0;
        currentSpouseScore += Spouse;
        if (currentSpouseScore < -10)
        {
            myWitsAdd -= Mathf.Max(5, -(currentSpouseScore + 10));
            currentSpouseScore = -10;
        }
        if (currentSpouseScore > 10)
        {
            myWitsAdd += Mathf.Max(5, currentSpouseScore - 10);
            currentSpouseScore = 10;
        }
        SpouseScore.text = currentSpouseScore.ToString("+##;-##;0");
        myWitsAdd = Mathf.Clamp(myWitsAdd, -5, +5);
        return myWitsAdd;
    }

    private int UpdateStaffScore(int Staff)
    {
        int myWitsAdd = 0;
        currentStaffScore += Staff;
        if (currentStaffScore < -10)
        {
            myWitsAdd -= Mathf.Max(5, -(currentStaffScore + 10));
            currentStaffScore = -10;
        }
        if (currentStaffScore > 10)
        {
            myWitsAdd += Mathf.Max(5, currentStaffScore - 10);
            currentStaffScore = 10;
        }
        StaffScore.text = currentStaffScore.ToString("+##;-##;0");
        myWitsAdd = Mathf.Clamp(myWitsAdd, -5, +5);
        return myWitsAdd;
    }
    private int UpdateHouseScore(int House)
    {
        int myWitsAdd = 0;
        currentHouseScore += House;
        if (currentHouseScore < -10)
        {
            myWitsAdd -= Mathf.Max(5, -(currentHouseScore + 10));
            currentHouseScore = -10;
        }
        if (currentHouseScore > 10)
        {
            myWitsAdd += Mathf.Max(5, currentHouseScore - 10);
            currentHouseScore = 10;
        }
        HouseScore.text = currentHouseScore.ToString("+##;-##;0");
        myWitsAdd = Mathf.Clamp(myWitsAdd, -5, +5);
        return myWitsAdd;
    }
}

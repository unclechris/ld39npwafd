﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using System;
[RequireComponent(typeof(CanvasGroup))]
public class Dragable : MonoBehaviour, IBeginDragHandler, IDragHandler,IEndDragHandler
{
    public enum Slot { Back , Visible };
    public Slot typeOfItem = Slot.Back;

    public Transform parentToReturnTo = null;
    public CanvasGroup myCanvasGroup;
    public bool removeAfterDrop = false;
    public bool deleteAfterDrop = false;

    public void Awake()
    {
        myCanvasGroup = this.GetComponent<CanvasGroup>();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        parentToReturnTo = this.transform.parent;
        DropZone myParentDropZone = parentToReturnTo.gameObject.GetComponent<DropZone>();
        if (myParentDropZone != null)
        {
            myParentDropZone.RemoveCard();
        }

        this.transform.SetParent(this.transform.parent.parent.parent);
        if (myCanvasGroup != null)
        {
            myCanvasGroup.blocksRaycasts = false;
        }
        this.transform.localScale *= 2;
        FindObjectOfType<AudioManager>().Play("cardpickup");
    }

    public void OnDrag(PointerEventData eventData)
    {
        this.transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("EndDrag");
        this.transform.localScale *= .5f;
        this.transform.SetParent(parentToReturnTo);
     //   DropZone myParentDropZone = parentToReturnTo.gameObject.GetComponent<DropZone>();
        parentToReturnTo = null;
        if (myCanvasGroup != null)
        {
            myCanvasGroup.blocksRaycasts = true;
        }
        FindObjectOfType<AudioManager>().Play("carddrop");
        if (deleteAfterDrop==true)
        {
            Destroy(this.transform.gameObject);
        }
        if (removeAfterDrop == true)
        {
            Destroy(this);
        }
    }
}

﻿using System;
using UnityEngine;

[System.Serializable]
public class YearlyEvent
{
    public static T RandomEnumValue<T>()
    {
        var v = Enum.GetValues(typeof(T));
        return (T)v.GetValue(new System.Random().Next(v.Length));
    }

    public bool Complete;
    public enum TypeOfInfluence { CHILD, SPOUSE, STAFF, HOUSE, WITS };
    public TypeOfInfluence Goal;
    public int GoalScore;
    public TypeOfInfluence Success;
    public int SuccessBonus;
    public TypeOfInfluence Failure;
    public int FailureReward;
    public string Title;
    public string Text;

    public static YearlyEvent CreateEventFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<YearlyEvent>(jsonString);
    }

    public string SaveToString()
    {
        return JsonUtility.ToJson(this);
    }
}
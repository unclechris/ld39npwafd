﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YearlyDeckManager : MonoBehaviour
{

    public List<YearlyEvent> YearlyEventDeck;
    public int NumberOfCards = 0;
    public int CardsToInitialize = 50;

    // Use this for initialization
    void Awake()
    {
        // load PlayerDeck with 50 Cards	
        YearlyEventDeck = new List<YearlyEvent>();
        LoadPlayerDeck();
        NumberOfCards = YearlyEventDeck.Count;
        ShuffleDeck();
    }

    void LoadPlayerDeck()
    {
        Queue<YearlyEvent> myEvents;
        myEvents = GetCards();
        while (myEvents.Count != 0)
        {
            YearlyEvent myEventCard = myEvents.Dequeue();
            myEventCard.FailureReward += Random.Range(-1, 1);
            myEventCard.SuccessBonus += Random.Range(-1, 1);

            YearlyEventDeck.Add(myEventCard);
            NumberOfCards = YearlyEventDeck.Count;
        }

    }

    public void ShuffleDeck()
    {
        YearlyEvent myCard;
        for (int i = 0; i < 1000; i++)
        {
            int card1 = Random.Range(0, YearlyEventDeck.Count - 1);
            int card2 = Random.Range(0, YearlyEventDeck.Count - 1);
            while (card1 == card2)
            {
                card2 = Random.Range(0, YearlyEventDeck.Count - 1);
            }
            myCard = YearlyEventDeck[card1];
            YearlyEventDeck[card1] = YearlyEventDeck[card2];
            YearlyEventDeck[card2] = myCard;
        }
    }

    public YearlyEvent DrawCard()
    {
        YearlyEvent retCard;
        retCard = YearlyEventDeck[0];
        YearlyEventDeck.RemoveAt(0);
        NumberOfCards = YearlyEventDeck.Count;
        return retCard;
    }

    public Queue<YearlyEvent> GetCards()
    {
        Queue<YearlyEvent> myEvents = new Queue<YearlyEvent>();

        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":0,\"GoalScore\":4,\"Success\":0,\"SuccessBonus\":3,\"Failure\":1,\"FailureReward\":-2,\"Title\":\"Care for Spouse\",\"Text\":\"You sacrifice your time with your kids to care for him.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":0,\"GoalScore\":4,\"Success\":0,\"SuccessBonus\":2,\"Failure\":4,\"FailureReward\":-3,\"Title\":\"Cheek Pinching Houseguests\",\"Text\":\"You sent her packing, the kids loved her when they were little.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":0,\"GoalScore\":6,\"Success\":1,\"SuccessBonus\":2,\"Failure\":4,\"FailureReward\":-3,\"Title\":\"Grandchild gets ill\",\"Text\":\"Consumption seems to be eating them alive.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":0,\"GoalScore\":4,\"Success\":4,\"SuccessBonus\":3,\"Failure\":4,\"FailureReward\":-2,\"Title\":\"What were they saying?\",\"Text\":\"You seem to be under the weather, time for children to dote on you.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":3,\"GoalScore\":4,\"Success\":4,\"SuccessBonus\":3,\"Failure\":1,\"FailureReward\":-2,\"Title\":\"Never liked the Dress.\",\"Text\":\"You misplaced a wedding photos.  \"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":3,\"GoalScore\":4,\"Success\":2,\"SuccessBonus\":3,\"Failure\":2,\"FailureReward\":-2,\"Title\":\"Raises for Staff\",\"Text\":\"Maybe those painting in the attic are worth something.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":3,\"GoalScore\":4,\"Success\":3,\"SuccessBonus\":2,\"Failure\":4,\"FailureReward\":-3,\"Title\":\"Fishy Houseguests\",\"Text\":\"You sent her packing, she can't be taking the silver.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":3,\"GoalScore\":6,\"Success\":3,\"SuccessBonus\":2,\"Failure\":4,\"FailureReward\":-2,\"Title\":\"Garden Party\",\"Text\":\"Your garden better be up to snuff, or you will go crazy.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":1,\"GoalScore\":4,\"Success\":1,\"SuccessBonus\":3,\"Failure\":0,\"FailureReward\":-2,\"Title\":\"Care for Spouse\",\"Text\":\"Your spouse was injured during a bull riding competition. Children will understand.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":1,\"GoalScore\":4,\"Success\":4,\"SuccessBonus\":3,\"Failure\":1,\"FailureReward\":-2,\"Title\":\"Obnoxious Best Friend\",\"Text\":\"You invited HER again to dinner, her laugh sends husband to smoking lounge.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":1,\"GoalScore\":6,\"Success\":2,\"SuccessBonus\":-2,\"Failure\":1,\"FailureReward\":-2,\"Title\":\"Cut Staff Wages\",\"Text\":\"You need to ask spouse for money, or maybe the Staff can take the hit.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":1,\"GoalScore\":6,\"Success\":2,\"SuccessBonus\":3,\"Failure\":2,\"FailureReward\":-2,\"Title\":\"Raises for Staff\",\"Text\":\"You think they have been working hard. You can convince your spouse.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":1,\"GoalScore\":4,\"Success\":0,\"SuccessBonus\":2,\"Failure\":4,\"FailureReward\":-3,\"Title\":\"Grandchild gets ill\",\"Text\":\"Your spouse is unhappy with favorite Grandchild being ill.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":1,\"GoalScore\":4,\"Success\":1,\"SuccessBonus\":2,\"Failure\":4,\"FailureReward\":-3,\"Title\":\"Obnoxious Houseguests\",\"Text\":\"You sent her packing, her laugh sends husband to smoking lounge.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":1,\"GoalScore\":4,\"Success\":4,\"SuccessBonus\":3,\"Failure\":4,\"FailureReward\":-2,\"Title\":\"What were they saying?\",\"Text\":\"You seem to be under the weather, your Spouse is strong material.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":1,\"GoalScore\":5,\"Success\":3,\"SuccessBonus\":2,\"Failure\":3,\"FailureReward\":-2,\"Title\":\"Foundation Cracks\",\"Text\":\"Maybe your spouse has a business contact to fix it.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":2,\"GoalScore\":4,\"Success\":3,\"SuccessBonus\":3,\"Failure\":3,\"FailureReward\":-2,\"Title\":\"Care for Spouse\",\"Text\":\"You ignore house to care for spouse, the staff has it.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":2,\"GoalScore\":6,\"Success\":4,\"SuccessBonus\":3,\"Failure\":2,\"FailureReward\":-2,\"Title\":\"Rude Best Friend\",\"Text\":\"You invited HER again to dinner, she is rather demanding.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":2,\"GoalScore\":6,\"Success\":3,\"SuccessBonus\":3,\"Failure\":2,\"FailureReward\":1,\"Title\":\"Cut Staff Wages\",\"Text\":\"You want to get new curtains, the staff will need to pitch in.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":2,\"GoalScore\":4,\"Success\":2,\"SuccessBonus\":2,\"Failure\":4,\"FailureReward\":-3,\"Title\":\"Rude Houseguests\",\"Text\":\"You sent her packing, she is rather demanding.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":2,\"GoalScore\":4,\"Success\":4,\"SuccessBonus\":3,\"Failure\":4,\"FailureReward\":-2,\"Title\":\"What were they saying?\",\"Text\":\"You seem to be under the weather, maybe the Staff can help.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":4,\"GoalScore\":10,\"Success\":1,\"SuccessBonus\":3,\"Failure\":0,\"FailureReward\":-2,\"Title\":\"Favored Staff Dies\",\"Text\":\"Your Spouse didn't like them, but your kids loved them.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":4,\"GoalScore\":12,\"Success\":2,\"SuccessBonus\":3,\"Failure\":0,\"FailureReward\":-2,\"Title\":\"Despied Staff Disappears\",\"Text\":\"Who will take care of the pet Alligators?\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":4,\"GoalScore\":12,\"Success\":4,\"SuccessBonus\":3,\"Failure\":0,\"FailureReward\":-2,\"Title\":\"Cheek pinching Best Fiend\",\"Text\":\"You invited HER again to dinner, the kids loved her when they were little.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":4,\"GoalScore\":12,\"Success\":2,\"SuccessBonus\":3,\"Failure\":3,\"FailureReward\":-2,\"Title\":\"Despied Staff Removed\",\"Text\":\"You take it upon yourself to clear ranks.  They just had to leave.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":4,\"GoalScore\":12,\"Success\":4,\"SuccessBonus\":3,\"Failure\":3,\"FailureReward\":-2,\"Title\":\"Fishy Best Friend\",\"Text\":\"You invited HER again to stay, she can't be taking the silver.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":4,\"GoalScore\":11,\"Success\":3,\"SuccessBonus\":3,\"Failure\":2,\"FailureReward\":-2,\"Title\":\"Favored Staff Quits\",\"Text\":\"Your favorite Staff quits, hope you can deal with it.\"}"));
        myEvents.Enqueue(YearlyEvent.CreateEventFromJSON("{\"Complete\":false,\"Goal\":4,\"GoalScore\":10,\"Success\":4,\"SuccessBonus\":3,\"Failure\":4,\"FailureReward\":-2,\"Title\":\"Quilting Bee\",\"Text\":\"You enter a project in Quilting Bee.\"}"));
        return myEvents;
    }

}

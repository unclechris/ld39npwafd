﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using System;

public class DropZone : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    public int NumOfItems = 0;
    public int MaxNumItems;
    Playfield myPlayfield;
    public bool isPlayfield;

    public void Awake()
    {
       myPlayfield = GetComponent<Playfield>();
        isPlayfield = (myPlayfield != null);
    }

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("OnDrop");
        if (CanAddCard())
        {
            Dragable myDragInfo = eventData.pointerDrag.GetComponent<Dragable>();
            if (myDragInfo != null)
            {
                myDragInfo.parentToReturnTo = gameObject.transform;
                AddCard();
                if (myPlayfield != null)
                {
                    myDragInfo.myCanvasGroup.blocksRaycasts = false;
                    myDragInfo.removeAfterDrop = true;
                    //Check if end of Year
                    if (NumOfItems==MaxNumItems)
                    {
                        myPlayfield.endOfTurn = true;
                    }
                    myPlayfield.UpdateScore(eventData.pointerDrag);

                }
            }
        }
    }
    
    public bool CanAddCard()
    {
        return (NumOfItems < MaxNumItems); 
    }
    private void AddCard()
    {
        NumOfItems += 1;
    }
    public void RemoveCard()
    {
        NumOfItems -= 1;
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
    }

    public void OnPointerExit(PointerEventData eventData)
    {
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Wits : MonoBehaviour {
    public int CurrentWits;
    public int MaxWits;
    public int StartingWits;
    public GameObject Pearl;
    public GameObject witsHolder;
    public List<GameObject> myPearls;
    public GameManger myGameManager;

	// Use this for initialization
	void Start() {
        SetCurWits(StartingWits);
	}

    private void SetCurWits(int newWits)
    {
        CurrentWits = 0;
        ClearPearls();
        while (CurrentWits < newWits)
        {
            AddPearl();
        }

    }

    private void ClearPearls()
    {
        foreach (Transform child in witsHolder.transform)
        {
            DestroyObject(child.gameObject);
        }
        myPearls.Clear();
    }

    private void AddPearl()
    {
        if (CurrentWits < MaxWits)
        {
            GameObject myNewPearl = Instantiate(Pearl);
            myNewPearl.transform.SetParent(witsHolder.transform, false);
            myPearls.Add(myNewPearl);
            CurrentWits += 1;
        }
    }

    private void RemovePearl()
    {
        if (witsHolder.transform.childCount>0)
        {
            CurrentWits -= 1;
            if (CurrentWits == 0)
            {
                myGameManager.SetStateDead();
            }
            GameObject gm = myPearls[0];
            myPearls.RemoveAt(0);
            Destroy(gm);
        }
    }

    public void UpdateWits(int newWits)
    {
        if (newWits != 0)
        {
            if (newWits <= -3)
            {
                FindObjectOfType<AudioManager>().Play("mypearls");
            }
            for (int i = 0; i < Mathf.Abs(newWits); i++)
            {
                if (newWits < 0)
                {
                    RemovePearl();
                }
                if (newWits > 0)
                {
                    AddPearl();
                }
            }
        }
    }

}

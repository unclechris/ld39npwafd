﻿using UnityEngine;

public class UpdateYearlyEvent : MonoBehaviour {

    public bool HasEvent = false;
    public GameObject myEventCard;
    public YearlyDeckManager myEventDeckManager;
    public Playfield myPlayfield;

    public void Start()
    {
        UpdateYearlyEventCard();
    }

    private GameObject GetMyDraw()
    {
        return Instantiate(myEventCard);
    }

    public void UpdateYearlyEventCard()
    {
        if (HasEvent == false)
        {
            GameObject myDraw = GetMyDraw();
            myDraw.transform.SetParent(this.transform, false);
            EventCardInfo myEventCardInfo = myDraw.GetComponent<EventCardInfo>();
            if (myEventCardInfo != null)
            {
                YearlyEvent myCard = myEventDeckManager.DrawCard();
                if (myPlayfield != null)
                {
                    myEventCardInfo.myPlayfield = myPlayfield;
                    myPlayfield.myEventCardInfo = myEventCardInfo;
                    myPlayfield.currentEvent = myCard;
                    myEventCardInfo.UpdateEventCardInfo(myCard);
                    HasEvent = true;
                    if (myPlayfield.myWits != null)
                    {
                        if (myPlayfield.myWits.CurrentWits < 5)
                        {
                            myEventCardInfo.UpdateEventForDementia(myPlayfield.myWits.CurrentWits);
                        }
                    }
                }
            }
        }
    }
}

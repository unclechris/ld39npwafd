﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Card
{
    public string Title;
    public string Effect;
    public int Child;
    public int Spouse;
    public int House;
    public int Staff;
    public int Wits;
    public bool isEvent;
    public Color myBackgroundColor;

    public static Card CreateCardFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<Card>(jsonString);
    }

    public string SaveToString()
    {
        return JsonUtility.ToJson(this);
    }
}

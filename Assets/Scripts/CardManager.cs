﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class CardManager : MonoBehaviour, IPointerClickHandler
{
    public int count = 0;
    public GameObject myCard;
    public GameObject myHand;
    public GameObject myDeck;
    public GameObject myFaceupCard;
    public Playfield myPlayfield;
    public GameObject myYearlyEvent;

    public PlayerDeckManager myPlayerDeck;
    public YearlyDeckManager myYearlyEventDeck;

    public int numCardsInHand = 7;
    private void Start()
    {
        
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        switch (eventData.pointerPress.tag)
        {
            case "Deck":
                HandleDeckClick();
                break;
            default:
                Debug.Log("pointer click" + eventData.pointerPress.tag);
                break;
        }
       
    }

    private void HandleDeckClick()
    {        
        if (myPlayfield.endOfTurn==true)
        {
            myPlayfield.EndTurn();
            //Unlock Deck
            count -= 4;
        }
        if (count < numCardsInHand)
        {
            GameObject myDraw = Instantiate(myCard);
            myDraw.transform.SetParent(myHand.transform, false);
            CardInfo myDrawCardInfo = myDraw.GetComponent<CardInfo>();
            if (myDrawCardInfo != null)
            {
                Card myCard = myPlayerDeck.DrawCard();
                myDrawCardInfo.UpdateCard(myCard);
                if (myPlayfield.myWits.CurrentWits <5)
                {
                    myDrawCardInfo.UpdateCardForDementia(myPlayfield.myWits.CurrentWits);
                }
            }
            Dragable myDrawDragable = myDraw.GetComponent<Dragable>();
            if (myDrawDragable != null)
            {
                myDrawDragable.parentToReturnTo = myHand.transform;
            }
            count += 1;
        }        
    }
}

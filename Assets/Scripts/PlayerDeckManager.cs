﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeckManager : MonoBehaviour
{

    public List<Card> PlayerDeck;
    public int NumberOfCards = 0;
    public int CardsToInitialize = 50;

    // Use this for initialization
    void Awake()
    {
        // load PlayerDeck with 50 Cards	
        PlayerDeck = new List<Card>();
        LoadPlayerDeck();
        NumberOfCards = PlayerDeck.Count;
        ShuffleDeck();
    }

    void LoadPlayerDeck()
    {
        Queue<Card> myEvents;
        myEvents = GetCards();
        while (myEvents.Count != 0)
        {
            Card myEventCard = myEvents.Dequeue();
           // myEventCard.Wits = -3 + Random.Range(-2,1);
            myEventCard.myBackgroundColor = Color.HSVToRGB(Random.Range(0.0f, 1.0f), 98.0f / 255.0f, 248.0f / 255.0f);
            PlayerDeck.Add(myEventCard);
            myEventCard.myBackgroundColor = Color.HSVToRGB(Random.Range(0.0f, 1.0f), 98.0f / 255.0f, 248.0f / 255.0f);
            PlayerDeck.Add(myEventCard);
        }
        NumberOfCards = PlayerDeck.Count;
    }

    public void ShuffleDeck()
    {
        Card myCard;
        for (int i = 0; i < 1000; i++)
        {
            int card1 = Random.Range(0, PlayerDeck.Count - 1);
            int card2 = Random.Range(0, PlayerDeck.Count - 1);
            while (card1 == card2)
            {
                card2 = Random.Range(0, PlayerDeck.Count - 1);
            }
            myCard = PlayerDeck[card1];
            PlayerDeck[card1] = PlayerDeck[card2];
            PlayerDeck[card2] = myCard;
        }
    }

    public Card DrawCard()
    {
        Card retCard;
        retCard = PlayerDeck[0];
        PlayerDeck.RemoveAt(0);
        NumberOfCards = PlayerDeck.Count;
        return retCard;
    }

    public Queue<Card> GetCards()
    {
        Queue<Card> myEvents = new Queue<Card>();

        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Spouse Work Dinner\",\"Effect\":\"Your Spouse needs you to host a dinner for boss.\",\"Child\":0,\"Spouse\":2,\"House\":0,\"Staff\":-2,\"Wits\":0,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Garden Party\",\"Effect\":\"You host the Garden party, and push staff to make house lovely.\",\"Child\":0,\"Spouse\":0,\"House\":2,\"Staff\":-2,\"Wits\":0,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Congratulate Staff\",\"Effect\":\"Your dinner last night was lovely, your staff really shined.\",\"Child\":0,\"Spouse\":0,\"House\":0,\"Staff\":2,\"Wits\":-1,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Dinner Party\",\"Effect\":\"Your dinner last night was lovely, without your children.\",\"Child\":-1,\"Spouse\":0,\"House\":0,\"Staff\":2,\"Wits\":-1,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Dinner Party\",\"Effect\":\"Your dinner with the spouse last night was lovely.\",\"Child\":-1,\"Spouse\":2,\"House\":0,\"Staff\":-2,\"Wits\":2,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Dinner Party\",\"Effect\":\"Your dinner was a failure, the house is a mess.\",\"Child\":3,\"Spouse\":-1,\"House\":-2,\"Staff\":-1,\"Wits\":0,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Bad Milk\",\"Effect\":\"Someone used bad milk in breakfast, children and Spouse are upset.\",\"Child\":-2,\"Spouse\":-1,\"House\":-1,\"Staff\":-1,\"Wits\":-1,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Tea Party\",\"Effect\":\"The Tea Party you hosted, was well received.\",\"Child\":-1,\"Spouse\":-1,\"House\":2,\"Staff\":-2,\"Wits\":2,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Window left open\",\"Effect\":\"A flock of birds caused all sorts of problems this morning.\",\"Child\":3,\"Spouse\":1,\"House\":-2,\"Staff\":-1,\"Wits\":-1,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Window left open\",\"Effect\":\"A flock of birds caused all sorts of problems this morning.\",\"Child\":-1,\"Spouse\":0,\"House\":-2,\"Staff\":0,\"Wits\":0,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Fight\",\"Effect\":\"Your Children had a fight, the house took the most damage.\",\"Child\":1,\"Spouse\":0,\"House\":-2,\"Staff\":0,\"Wits\":-1,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Fight\",\"Effect\":\"Your Children had a fight, the house took the most damage.\",\"Child\":-1,\"Spouse\":0,\"House\":-2,\"Staff\":0,\"Wits\":1,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Butt of Gossip\",\"Effect\":\"You were talked about all week.  Thank heavens it was good.\",\"Child\":0,\"Spouse\":1,\"House\":0,\"Staff\":2,\"Wits\":2,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Butt of Gossip\",\"Effect\":\"You were talked about all week.  It could have been better.\",\"Child\":1,\"Spouse\":0,\"House\":0,\"Staff\":1,\"Wits\":0,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Butt of Gossip\",\"Effect\":\"You were talked about all week.  This week couldn't end quick enough.\",\"Child\":0,\"Spouse\":2,\"House\":0,\"Staff\":-1,\"Wits\":-2,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Goodhousekeeping Award\",\"Effect\":\"Your house was the talk of the Social circle.  The staff really put in a fine show.\",\"Child\":0,\"Spouse\":0,\"House\":2,\"Staff\":2,\"Wits\":0,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Goodhousekeeping Award\",\"Effect\":\"Your house was the talk of the Social circle, yelling at the staff has paid off.\",\"Child\":0,\"Spouse\":0,\"House\":2,\"Staff\":-2,\"Wits\":0,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Exploision\",\"Effect\":\"Science projects are most dangerous items\",\"Child\":2,\"Spouse\":0,\"House\":-2,\"Staff\":-1,\"Wits\":2,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Babysit\",\"Effect\":\"You babysit the grandchildren.  They are such dears.\",\"Child\":3,\"Spouse\":0,\"House\":-3,\"Staff\":1,\"Wits\":2,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Babysit\",\"Effect\":\"You babysit the grandchildren.  What was I thinking.\",\"Child\":-2,\"Spouse\":0,\"House\":-3,\"Staff\":-1,\"Wits\":-2,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Babysit\",\"Effect\":\"You babysit the grandchildren.  They torn up your husband's lounge.\",\"Child\":-1,\"Spouse\":-3,\"House\":0,\"Staff\":-1,\"Wits\":-1,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Quiet Afternoon\",\"Effect\":\"Not a care in the world.\",\"Child\":0,\"Spouse\":0,\"House\":0,\"Staff\":0,\"Wits\":2,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Quiet Afternoon\",\"Effect\":\"Not a care in the world. Staff is off for day.\",\"Child\":0,\"Spouse\":-1,\"House\":0,\"Staff\":2,\"Wits\":1,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Quiet Afternoon\",\"Effect\":\"Not a care in the world.\",\"Child\":0,\"Spouse\":0,\"House\":0,\"Staff\":0,\"Wits\":2,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Quite the Afternoon\",\"Effect\":\"Take me somewhere sane!\",\"Child\":0,\"Spouse\":0,\"House\":0,\"Staff\":-2,\"Wits\":-4,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Holy Vows\",\"Effect\":\"Children get married.  It was simply divine.\",\"Child\":2,\"Spouse\":2,\"House\":0,\"Staff\":2,\"Wits\":0,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        myEvents.Enqueue(Card.CreateCardFromJSON("{\"Title\":\"Holy Vows\",\"Effect\":\"Children get married.  It was far less than expected.\",\"Child\":2,\"Spouse\":0,\"House\":0,\"Staff\":-1,\"Wits\":-1,\"isEvent\":false,\"myBackgroundColor\":{\"r\":0,\"g\":0,\"b\":0,\"a\":1.0}}"));
        return myEvents;
    }

}

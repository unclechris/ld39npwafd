﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManger : MonoBehaviour {

    public Transform Instructions;
    public Text txtInstructions;
    public Text txtButton1;
    public Text txtButton2;
    public Animator myAnimator;
    public PlayerDeckManager myPlayerDeckMgr;
    public YearlyDeckManager myYearlyDeckMgr;
    public Playfield myPlayfield;
    public AudioManager myAudioManager;

    string State = "begin";

	// Use this for initialization
	void Start () {
        SetStateBegin();
        myAudioManager = FindObjectOfType<AudioManager>();
    }

    void SetStateBegin()
    {
        txtInstructions.text = "You are playing Lady Allyson Brecken-Smythe of the Hamptonshire Brecken-Smythes.You are near the end of your long life, and can only last as long as you have pearls of wisdom to give.  Dementia runs in the family, so be aware it will strike you as you start losing your wits.\n\nClick the Deck to draw your cards.";
        State = "begin";
        txtButton1.text = "Instructions";
        txtButton2.text = "Play";
        myAnimator.SetBool("IsOpen", true);

    }

    public void SetStateEndOfTurn()
    {
        string myScoreSheet = "It is now " + (myPlayfield.TurnCount + 1880).ToString() + ".\n" +
                              "Your turn has ended, and you are a year older!\n" +
                              "You did";
        if (myPlayfield.currentEvent.Complete==false)
        {
            myScoreSheet = myScoreSheet + " not";
            if (Random.Range(0.0f, 1.0f) <= 0.5f)
            {
                myAudioManager.Play("notgood1");
            }
            else
            {
                myAudioManager.Play("notgood2");
            }
        }
        else
        {
            myAudioManager.Play("wellplayed");
        }
        myScoreSheet = myScoreSheet + " complete your Event successfully.\n\n";
        myScoreSheet = myScoreSheet + "Your current total score is " + myPlayfield.currentTotalScore.ToString("+###;-###;0");
        txtInstructions.text = myScoreSheet;
        State = "EndOfTurn";
        txtButton1.text = "Instructions";
        txtButton2.text = "Continue";
        myAnimator.SetBool("IsOpen", true);
    }

    public void SetStateDead()
    {
        string myScoreSheet = "Sadly, you have lost all your sanity, and your family has come to take you away to a place where you won't bother anyone anymore.\n";
        myScoreSheet = myScoreSheet + "Your total score was " + myPlayfield.currentTotalScore.ToString("+###;-###;0");
        txtInstructions.text = myScoreSheet;
        State = "Dead";
        txtButton1.text = "Quit";
        txtButton2.text = "Replay";
        myAnimator.SetBool("IsOpen", true);
        myAudioManager.Play("death");
        myAudioManager.Play("deathdoorslam");

    }
    public void ClickButtonOne()
    {
        switch (State)
        {
            case "begin":
                // Display Instructions
                // Set button one to Continue
                txtButton1.text = "Continue";
                State = "inst1";
                txtInstructions.text = "You have a hand of 7 Cards, and play one at a time." +
                    "\nYou play 4 cards in order in a round, which is a year. " +
                    "\nYour total score is the score of your Children's, Spouse's, Staff's, and House's influence.";                    
                break;
            case "inst1":
                txtButton1.text = "Continue";
                State = "inst2";
                txtInstructions.text = "If you accomplish the goal on the Yearly Event before the end of year, then you will get the Success otherwise you will get the fail outcome.";
                break;
            case "inst2":
                txtButton1.text = "Quit";
                State = "Play";
                txtInstructions.text = "As you loose your marbles, you will probably notice things getting more difficult to focuse due to the dementia kicking in.";
                break;
            case "Play":
            case "Dead":
            case "Pause":
                State = "";
                myAudioManager.Stop("rocking");
                myAudioManager.Stop("death");
                myAudioManager.Stop("deathdoorslam");

                myAnimator.SetBool("IsOpen", false);
                SceneManager.LoadScene(0);
                break;
            default:
                break; 
        }
    }

    public void ClickButtonTwo()
    {
        switch (State)
        {
            case "begin":
            case "inst1":
            case "inst2":
            case "Play":
            case "EndOfTurn":
                // play clicked - make canvas go away, draw Yearly Event 
                // and enable canvas group on deck
                myAnimator.SetBool("IsOpen", false);
                break;
            case "Pause":
                // play clicked - make canvas go away, draw Yearly Event 
                // and enable canvas group on deck
                myAudioManager.Stop("rocking");
                myAnimator.SetBool("IsOpen", false);
                break;
            case "Dead":
                State = "";
                myAudioManager.Stop("death");
                myAudioManager.Stop("deathdoorslam");
                myAnimator.SetBool("IsOpen", false);
                SceneManager.LoadScene(1);
                break;
            default:
                break;
        }

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)==true)
        {
            State = "Pause";
            txtButton1.text ="Quit";
            txtButton2.text = "Resume";
            txtInstructions.text = "Game currently Paused.";
            myAnimator.SetBool("IsOpen", true);
            myAudioManager.Play("rocking");
        }

        if (Input.GetKeyDown(KeyCode.Q) == true)
        {
            Debug.Log("Questions?");
            SetStateBegin();
        }

    }
}

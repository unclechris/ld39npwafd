﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class EventCardInfo : MonoBehaviour {

    public Image imgGoal;
    public Text txtGoal;
    public Image imgGoalSuccess;
    public Image imgSuccess;
    public Text txtSuccess;
    public Image imgFailure;
    public Text txtFailure;
    public Text txtTitle;
    public Text txtFlavor;

    public YearlyEvent myYearlyEvent;
    
    public Sprite Child;
    public Sprite House;
    public Sprite Spouse;
    public Sprite Staff;
    public Sprite Wits;
    public Playfield myPlayfield;
    
    private Sprite InfluenceToSprite(YearlyEvent.TypeOfInfluence myInfluence)
    {
        Sprite returnSprite = null;

        switch (myInfluence)
        {
            case YearlyEvent.TypeOfInfluence.CHILD:
                returnSprite = Child;
                break;
            case YearlyEvent.TypeOfInfluence.HOUSE:
                returnSprite = House;
                break;
            case YearlyEvent.TypeOfInfluence.SPOUSE:
                returnSprite = Spouse;
                break;
            case YearlyEvent.TypeOfInfluence.STAFF:
                returnSprite = Staff;
                break;
            case YearlyEvent.TypeOfInfluence.WITS:
                returnSprite = Wits;
                break;
            default:
                break;
        }
        return returnSprite;
    }
    public void UpdateEventCardInfo()
    {
        UpdateEventCardInfo(myYearlyEvent);
    }
    public void UpdateEventCardInfo(YearlyEvent myEvent)
    {
        if (myEvent != null)
        {
            myYearlyEvent = myEvent;
            txtTitle.text = myEvent.Title;
            txtFlavor.text = myEvent.Text;
            if (myEvent.Complete == true)
            {
                imgGoalSuccess.color = Color.green;
            }
            else
            {
                imgGoalSuccess.color = Color.red;
            }
            txtGoal.text = myEvent.GoalScore.ToString("+###;-###;0");
            txtSuccess.text = myEvent.SuccessBonus.ToString("+###;-###;0");
            txtFailure.text = myEvent.FailureReward.ToString("+###;-###;0");
            imgGoal.sprite = InfluenceToSprite(myEvent.Goal);
            imgFailure.sprite = InfluenceToSprite(myEvent.Failure);
            imgSuccess.sprite = InfluenceToSprite(myEvent.Success);
        }
    }

    public void UpdateEventForDementia(int Wits)
    {
        txtTitle.text = UpdateStringForDementia(txtTitle.text, Wits);
        txtFlavor.text = UpdateStringForDementia(txtFlavor.text, Wits);
        txtGoal.text = UpdateStringForDementia(txtGoal.text, Wits);
        txtSuccess.text = UpdateStringForDementia(txtSuccess.text, Wits);
        txtFailure.text = UpdateStringForDementia(txtFailure.text, Wits);
    }

    public string UpdateStringForDementia(string myString, int Wits)
    {
        string newString = "";
        foreach (char c in myString)
        {
            char newLetter = c;
            if (Random.Range(0.0f, 1.0f) <= ((10 - Wits) * .03f))
            {
                int num = Random.Range(0, 27);
                if (num == 27)
                {
                    newLetter = ' ';
                }
                else
                {
                    newLetter = (char)('a' + num);
                }
                if (Random.Range(0.0f, 1.0f) <= .07f)
                {
                    newLetter = char.ToUpper(newLetter);
                }
            }

            newString = newString + newLetter;
        }
        Debug.LogWarning(myString + "=>" + newString);
        return newString;
    }

}
